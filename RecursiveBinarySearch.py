# A recursive method that takes in a list of integers, number to search, the first and last index of the list
def recursive_binary_search(list_of_integers, search_key, first_index, last_index):

    # The position of the middle number gotten from adding the first and last position and dividing by 2
    middle_index = first_index + (last_index - first_index)//2

    # A nested IF that decreases the search space depending on the position of the number been searched in the list
    if last_index >= first_index:

        # Return the middle index if the searched number is found at that position
        if search_key == list_of_integers[middle_index]:
            return middle_index

        # If the searched number is greater than the number at the middle position, shrink the search space by
        # disregarding the numbers which are below the value of the searched number

        elif search_key > list_of_integers[middle_index]:
            return recursive_binary_search(list_of_integers, search_key, middle_index + 1, last_index)

        # If the searched number is less than the number at the middle position, reduce the search space by
        # disregarding the numbers which are above the searched number.
        else:
            return recursive_binary_search(list_of_integers, search_key, first_index, middle_index - 1)

    # At this point, the number has not been found in the list
    else:
       return -1

# Declare and Initialize a list for test running the program
list_of_integers = [-20, -9, -1, 1, 2, 2, 3, 4, 5, 60, 90]

# Search for this value
search_key = 60

# Make a recursive search and pass the current index containing the searched number to the index variable
index = recursive_binary_search(list_of_integers, search_key, first_index=0, last_index=len(list_of_integers) - 1)

# What to print out if the searched number is found
if index != -1:
    print("The number %d was found at %d " % (search_key, index))

# What to print out if the searched number is not found
else:
    print("The number " + str(search_key) + " was not found in the list")

